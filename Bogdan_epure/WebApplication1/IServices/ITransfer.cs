﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreAngular.IServices
{
    public interface ITransfer
    {
        Task Get();
        Task Send(ArraySegment<byte> buffer);
    }
}
