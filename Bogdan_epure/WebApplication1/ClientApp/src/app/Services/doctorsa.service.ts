import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebsocketService } from './wsservice.service';

@Injectable({
  providedIn: 'root'
})
export class DoctorsaService implements OnInit{

    private counter: number = 0;

    constructor(private http: HttpClient, private wsService: WebsocketService) { }

    ngOnInit() {
        this.wsService.connect("ws://localhost:50130/appcl/");
    }

    getUserRole(email: string) {
        var body = {
            "email": "ep.bogdy@gmail.com"
        }
        //this.wsService.connect("wss://echo.websocket.org/");
        if (this.counter == 0) {
            this.wsService.connect("ws://localhost:50130/appcl/");
            this.counter++;
        }
          
        return this.http.get('appcl/getUserRole', { params: { email: email } });
    }

    getAllPatients() {
        return this.http.get('appcl/getAllPatients');
    }

    deletePatient(patientName:string, patientId:string) {
        let params = '?userId=' + patientId;
        return this.http.post('appcl/deletePatient' + params, null)
    }

    addPrescription(prescription: string, userId:string) {
        let params = '?presc=' + prescription +'&userId='+userId;
        return this.http.post('appcl/presPatient' + params,null);
    }

    getPatientPrescription(email: string) {
        return this.http.get('appcl/getPrescription', { params: { email: email } });
    }
}
